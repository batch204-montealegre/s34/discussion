// require directive is use to load the express module/packages.
// It allows us to access the methods and function in easily creating our server.
const express = require("express");

// This creates an express application and stores this in a constant called app.
// In layman's term, app is our server.
const app = express();

// Setup for allowing the server to handle data from requests
// Methods used express JS are middleware
	// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
app.use(express.json()); // Allows your app to read json data.

// For our application server to run, we need a port to listne to
const port = 3000;

// Express has methods corresponding to each HTTP method.
// "/" corresponds with our base URI
// localhost:3000/
app.get("/", (req, res) =>{
	// res.send uses the express JS module's method to send a response back to the client.
	res.send("Hello World");
});


app.post("/hello", (req, res) =>{
	// res.send uses the express JS module's method to send a response back to the client.
	console.log(req.body);
	res.send(`Hello There ${req.body.firstName} ${req.body.lastName}!`);

});

let users = [
		{
			username: "janedoe",
			password: "jane123"
		},
		{
			username: "johnsmith",
			password: "john123"
		}
	];

app.post("/signup", (req, res) =>{
	console.log(req.body);

	if(req.body.username !==""&& req.body.password !== "" &&req.body.username !== undefined && req.body.password !== undefined){

		users.push(req.body);
		res.send(`User ${req.body.username} is successfully registered!`)
		}

		else{
			res.send(`Please fill out both username and password`);
		}
	});

app.get("/users", (req,res) => {
	res.send(users);
});

app.put("/change-password", (req, res)=> {
	let message;

	for (let i= 0; i<users.length; i++){
		if(users[i].username === req.body.username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated`

			break;
		}

		else{

			message = `User does not exist!`;
		}
	}

	res.send(message);
})




app.delete("/delete-user", (req, res)=> {
	let message;

	for (let i= 0; i<users.length; i++){
		if(users[i].username === req.body.username){
			users.splice(users[i], 1)

			message = `User ${req.body.username}'s successfully deleted`

			break;
		}

		else{

			message = `User does not exist!`;
		}
	}

	res.send(message);
})


// Returns a message to confirm that the server is running in the terminal.
app.listen(port, () => console.log(`Server is running at port: ${port}`));